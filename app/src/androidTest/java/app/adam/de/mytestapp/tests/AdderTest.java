package app.adam.de.mytestapp.tests;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import app.adam.de.mytestapp.MyAdded;

/**
 * Created by Adam on 12.08.2014.
 */
public class AdderTest extends AndroidTestCase {


    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @SmallTest
    public void testNumberAdder() {
        int result = MyAdded.add(1, 5);
        assertEquals(6, result);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
