package app.adam.de.mytestapp.tests;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;

import app.adam.de.mytestapp.MainActivity;
import app.adam.de.mytestapp.R;

/**
 * Created by Adam on 21.08.2014.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Activity mActivity;
    private Button bt1, bt2, bt3;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        mActivity = getActivity();

        bt1 = (Button) mActivity.findViewById(R.id.button1);
        bt2 = (Button) mActivity.findViewById(R.id.button2);
        bt3 = (Button) mActivity.findViewById(R.id.button3);
    }

    public void testButtonsNotNull() {
        assertNotNull(bt1);
        assertNotNull(bt2);
        assertNotNull(bt3);
    }

    public void testButtonsHaveClickListener() {

        assertTrue(bt1.hasOnClickListeners());
        assertTrue(bt2.hasOnClickListeners());
        assertTrue(bt3.hasOnClickListeners());

    }
}
